package name

import "gitlab.com/zaunerc-trainings-public/code000/hashlib/hash"

func Create() string {
	return hash.Generate()
}
